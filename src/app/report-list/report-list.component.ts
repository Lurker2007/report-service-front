import {Component, Injectable, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ReportInfo} from "../service/data";

@Component({
  selector: 'app-report-list',
  templateUrl: './report-list.component.html',
  styleUrls: ['./report-list.component.css']
})

@Injectable()
export class ReportListComponent implements OnInit {
  private reportInfoList: ReportInfo[];

  constructor(private httpClient: HttpClient) { }

  ngOnInit() {
   this.httpClient.get<ReportInfo[]>('http://localhost:8080/').subscribe(data => {
     this.reportInfoList = data;
    });
  }
}
