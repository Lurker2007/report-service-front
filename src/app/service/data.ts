export interface ReportInfo {
  id: number;
  name: string;
}

export interface ReportConditionModel {
  id: number;
  name: string;
  value: any;
  type: string;
}

export interface ReportFieldModel {
  name: string;
  isEnable: boolean;
}

export interface ReportModel {
  id: number;
  name: string;
  reportConditions: ReportConditionModel[];
  reportFields: ReportFieldModel[];
}
