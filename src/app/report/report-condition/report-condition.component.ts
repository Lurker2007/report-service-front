import {Component, Input, OnInit} from '@angular/core';
import {ReportConditionModel, ReportFieldModel} from "../../service/data";

@Component({
  selector: 'app-report-condition',
  templateUrl: './report-condition.component.html',
  styleUrls: ['./report-condition.component.css']
})
export class ReportConditionComponent implements OnInit {
  @Input() reportCondition: ReportConditionModel;
  constructor() { }

  ngOnInit() {
  }

  public updateConditionFieldValue(reportCondition, event) {
    reportCondition.value = event.target.value;
  }
}
