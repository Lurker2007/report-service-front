import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportConditionComponent } from './report-condition.component';

describe('ReportConditionComponent', () => {
  let component: ReportConditionComponent;
  let fixture: ComponentFixture<ReportConditionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportConditionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportConditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
