import {Component, Injectable, Input, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute} from "@angular/router";
import {ReportFieldModel, ReportModel} from "../service/data";

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})

@Injectable()
export class ReportComponent implements OnInit {
  private report: ReportModel;

  constructor(
    private httpClient: HttpClient,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    let param = this.route.snapshot.paramMap.get('id');
    this.httpClient.get<ReportModel>('http://localhost:8080/' + param).subscribe(data => {
      console.log(data);
      this.report = data;
    });
  }

  public updateReportField(reportField: ReportFieldModel, event) {
    reportField.isEnable = event.target.checked;
  }

  downloadFile(data, type) {
    let fileType = "";
    if (type === "xls") {
      fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    } else {
      fileType = 'application/pdf';
    }
    const blob = new Blob([data], {type: fileType});
    const url = window.URL.createObjectURL(blob);
    this.startDownload(url);
  }

  public download(type) {
    let restUrl = 'http://localhost:8080/download/' + type;
    this.httpClient.post(restUrl, this.report, {responseType: "blob"})
      .subscribe(data => this.downloadFile(data, type)
      );
    console.log(this.report);
  }

  startDownload(url) {
    window.location.href = url;
  }
}
