import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ReportListComponent} from "./report-list/report-list.component";
import {ReportComponent} from "./report/report.component";

const routes: Routes = [{path: '', component: ReportListComponent, pathMatch: 'full'},
  {
    path: 'report/:id',
    component: ReportComponent
  //   children: [
  //     { path: '', component: ThreadsComponent },
  //     { path: '/data', component: ThreadComponent }
  //   ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
